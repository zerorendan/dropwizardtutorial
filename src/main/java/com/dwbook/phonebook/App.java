package com.dwbook.phonebook;

import io.dropwizard.Application;
import io.dropwizard.auth.AuthFactory;
import io.dropwizard.auth.CachingAuthenticator;
import io.dropwizard.auth.basic.BasicAuthFactory;
import io.dropwizard.auth.basic.BasicCredentials;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;

import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dwbook.phonebook.resources.ContactResource;
import com.google.common.cache.CacheBuilderSpec;

public class App extends Application<PhonebookConfiguration> {

    private static final String DATA_BASE_MYSQL = "mysql";
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    @Override
    public void initialize(io.dropwizard.setup.Bootstrap<PhonebookConfiguration> bootstrap) {

    }

    @Override
    public void run(PhonebookConfiguration configuration, Environment environment) throws Exception {
        LOGGER.info("Method App#run() called");
        for (int i = 0; i < configuration.getMessageRepetitions(); i++) {
            System.out.println(configuration.getMessage());

        }
        final DBIFactory FACTORY = new DBIFactory();
        final DBI JDBI = FACTORY.build(environment, configuration.getDataSourceFactory(), DATA_BASE_MYSQL);

        // Authenticator, with caching support (CachingAuthenticator)
        environment.jersey().register(new ContactResource(JDBI, environment.getValidator()));
        CachingAuthenticator<BasicCredentials, Boolean> authenticator = new CachingAuthenticator<BasicCredentials, Boolean>(environment.metrics(), new PhonebookAuthenticator(JDBI), CacheBuilderSpec.parse("maximumSize=10000, expireAfterAccess=10m"));
        environment.jersey().register(AuthFactory.binder(new BasicAuthFactory<Boolean>(new PhonebookAuthenticator(JDBI), "Web Service Realm", Boolean.class)));

    }

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

}
