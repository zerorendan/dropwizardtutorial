package com.dwbook.phonebook.dao;

import org.skife.jdbi.v2.sqlobject.Bind;

import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

import com.dwbook.phonebook.dao.mappers.ContactMapper;
import com.dwbook.phonebook.representations.Contact;



public interface ContactDAO {
    @Mapper(ContactMapper.class)
    @SqlQuery("SELECT * FROM CONTACT WHERE id = :id")
    Contact getContactById(@Bind("id") int id);

    @GetGeneratedKeys
    @SqlUpdate("INSERT INTO CONTACT (id, firstName, lastName,  phone) values (NULL, :firstName, :lastName, :phone)")
    int createContact(@Bind("firstName") String firstName, @Bind("lastName") String lastName, @Bind("phone") String phone);

    @SqlUpdate("UPDATE CONTACT SET firstName = :firstName, lastName = :lastName, phone = :phone WHERE id = :id")
    void updateContact(@Bind("id") int id, @Bind("firstName") String firstName, @Bind("lastName") String lastName, @Bind("phone") String phone);

    @SqlUpdate("DELETE FROM CONTACT WHERE id = :id")
    void deleteContact(@Bind("id") int id);
    
    
}
