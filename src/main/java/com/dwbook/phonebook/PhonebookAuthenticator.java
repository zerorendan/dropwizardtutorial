package com.dwbook.phonebook;

import org.skife.jdbi.v2.DBI;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

import com.google.common.base.Optional;
import com.dwbook.phonebook.dao.UserDao;

public class PhonebookAuthenticator implements Authenticator<BasicCredentials, Boolean> {
    private final UserDao userDao;

    public PhonebookAuthenticator(DBI jdbi) {
        userDao = jdbi.onDemand(UserDao.class);
    }

    public Optional<Boolean> authenticate(BasicCredentials basicCredentials) throws AuthenticationException {
        boolean validUser = (userDao.getUser(basicCredentials.getUsername(), basicCredentials.getPassword()) == 1);
        if (validUser) {
            return Optional.of(true);
        }

        return Optional.absent();
    }

}
